
//  // student constructor
// function  Student (name,className){
//     this.name = name ;
//     this.class = className;
// }

// // Student Constructor Property which are access by all object of Student

// Student.prototype.intro = function () {
//     return `my name is ${this.name} and I'm study in class ${this.class}`;
// }

// const ram = new Student("Ram","7th");
// const shyam = new Student("Shyam","12th");
// console.log(ram.intro());
// console.log(shyam.intro());


// ES6 class

class Student {
   constructor(name,className){
       this.name = name;
       this.class = className;
   }

   intro() {
    return `my name is ${this.name} and I'm study in class ${this.class}`; 
   }

   // getter 
   get onlyname (){
    return `my name is ${this.name} `; 
   }
   //setter
   set rename(name) {
       this.name = name;
   }

   static collageName (){
        return `My collage is ECB`;
   }
}

const ram = new Student("Ram","7th");
const shyam = new Student("Shyam","12th");
console.log(ram.intro());
console.log(shyam.onlyname);
console.log(shyam.rename = "BigB");
console.log(shyam.onlyname);
console.log(Student.collageName());

